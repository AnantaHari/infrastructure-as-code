variable "yc_token" {
   default = ""
 }
variable "yc_cloud_id" {
  default = ""
}
variable "yc_folder_id" {
  default = "b1g0js1146c55lamm0g7"
}
variable "yc_region" {
  default = "ru-central1-a"
}
variable "yc_project_name" {
  default = "default"
}
variable "yc_network_id" {
  default = "enp09opd7ksaedcorclb"
}
variable "yc_subnet_id" {
  default = "e9bjnga63shpd7873vq0"
}
variable "yc_zone" {
  default = "ru-central1-a"
}

